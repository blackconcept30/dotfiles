# Installing Dependencies and Applications
sudo pacman -Syy xorg xmonad xorg-xinit xmonad-contrib xmobar git firefox alacritty picom nitrogen chromium dmenu conky rofi lf based-devel os-prober pamixer openssh onlyoffice-bin dunst

# Installing yay (AUR Helper)
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si

# Fonts
yay -S ttf-ms-fonts nerd-fonts-complete 

# Grub Dual Boot (os-prober)
sudo vim /etc/default/grub
uncomment -> GRUB_DISABLE_OS_PROBER=false
sudo grub2-mkconfig -o /boot/grub2/grub.cfg

# Trash Management
sudo pacman -S trash-cli
